export default {
  target: 'server',
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'papilion',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { href: "https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css", rel: "stylesheet" },
      { rel:"stylesheet", href:"/style.css" },
      { href:"https://fonts.googleapis.com/css2?family=Abril+Fatface&family=Gothic+A1:wght@200;800&display=swap", rel:"stylesheet" },
      { href:"https://fonts.googleapis.com/css2?family=Abril+Fatface&display=swap", rel:"stylesheet" },
      { rel:"stylesheet", href:"https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" },
    ],
    script: [
      { href: "/main.js" },
      { href: "https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js", integrity:"sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p", crossorigin:"anonymous" },
      { href: "https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js", integrity:"sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT", crossorigin:"anonymous" },
    ],
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
    // { src: './style.css', lang: 'css' }
  ],
  build: {
  },
  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/eslint
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [],

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  serverMiddleware: [
    '~/api/index.js'
  ]
}
