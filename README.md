# Referencias

- https://dev.to/prisma/adding-an-api-and-database-to-your-nuxt-app-with-prisma-2nlp

- https://github.com/ruheni/prisma-nuxt

## node version

```
source ~/.nvm/nvm.sh
nvm use --lts
```

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

## Prisma

```
nvm use --lts
npx prisma studio
```
